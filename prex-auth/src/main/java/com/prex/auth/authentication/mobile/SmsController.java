package com.prex.auth.authentication.mobile;

import cn.hutool.core.util.ObjectUtil;
import com.prex.common.message.sms.aliyuncs.util.AliYunSmsUtils;
import com.prex.common.message.sms.domain.SmsResponse;
import com.prex.common.core.constant.PrexConstant;
import com.prex.common.core.utils.R;
import com.prex.common.redis.util.PrexRedisUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Classname SmsController
 * @Description 短信控制层
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-08-21 15:42
 * @Version 1.0
 */
@RestController
@Api("发短信")
public class SmsController {

    @Autowired
    private AliYunSmsUtils aliYunSmsUtils;


    /**
     * 发送短信验证码
     *
     * @param phone
     * @return
     */
    @ApiOperation("发短信")
    @PostMapping("/sendCode/{phone}")
    public R sendSmsCode(@PathVariable("phone") String phone) {
        // 使用阿里短信发送
        SmsResponse smsResponse = aliYunSmsUtils.sendSms(phone, "prex", "登录");
        if (ObjectUtil.isNull(smsResponse)) {
            return R.error("短信发送失败");
        }
        // 保存到验证码到 redis 有效期两分钟
        PrexRedisUtil.set(PrexConstant.SMS_KEY + phone, smsResponse.getSmsCode(), 2);
        return R.ok();
    }
}
