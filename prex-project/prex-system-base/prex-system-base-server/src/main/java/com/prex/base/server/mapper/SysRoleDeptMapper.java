package com.prex.base.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.prex.base.api.entity.SysRoleDept;

/**
 * <p>
 * 角色与部门对应关系 Mapper 接口
 * </p>
 *
 * @author lihaodong
 * @since 2019-04-21
 */
public interface SysRoleDeptMapper extends BaseMapper<SysRoleDept> {

}
